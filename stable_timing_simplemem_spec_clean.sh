#!/bin/bash

SUFFIX=.fast

PROTOCOL=MESI_Two_Level

PYTHON='se_loop_clean.py'

gem5_home='.'
R=5

num_cpus=16

mesh_type='MeshDirCorners'

mesh_rows=4

num_l2=16

CPUMODE='timing'
MEMMODE='SimpleMemory'

MEM_LATENCY=200

L1D_SIZE=16kB #32
L1I_SIZE=16kB #32
L2_SIZE=256kB #512 
L1D_ASSOC=4
L1I_ASSOC=4
L2_ASSOC=8

#mem_size=8GB

#l2linesize=64

#MENU choose splash2 benchmark
PS3='Please enter your choice: '
options=("MCF" "HMMER" "SOPLEX" "GOBMK" "NAMD" "H264" "GROMACS" "OMNETPP" "ASTAR"  "LESLIE3D" "LBM" "GEMS" "ALL" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "MCF")
            scelta=1
            break
            ;;
        "HMMER")
            scelta=2
            break
            ;;
        "SOPLEX")
            scelta=3
            break
            ;;
        "GOBMK")
            scelta=4
            break
            ;;


        "NAMD")
            scelta=6
            break
            ;;
        "H264")
            scelta=7
            break
            ;;
        "GROMACS")
            scelta=8
            break
            ;;


        "OMNETPP")
            scelta=10
            break
            ;;

        "ASTAR")
            scelta=12
            break
            ;;

        "LESLIE3D")
            scelta=14
            break
            ;;
        "LBM")
            scelta=15
            break
            ;;
        "GEMS")
            scelta=16
            break
            ;;


        "ALL")
            scelta=99
            break
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done

###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

if [ $scelta -eq 1 ] || [ $scelta -eq 99 ]; then
echo "MCF"

benchmark="../spec2006CPU/1_mcf"
opt="../spec2006CPU/1_mcf_test.input"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="MCF_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 


###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 2 ] || [ $scelta -eq 99 ]; then
echo "HMMER"

benchmark="../spec2006CPU/2_hmmer"
opt="../spec2006CPU/2_hmmer_test.input"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="HMMER_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 3 ] || [ $scelta -eq 99 ]; then
echo "SOPLEX"

benchmark="../spec2006CPU/3_soplex"
opt="../spec2006CPU/3_soplex_test.input"


time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="SOPLEX_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 4 ] || [ $scelta -eq 99 ]; then
echo "GOBMK"

benchmark="../spec2006CPU/4_gobmk"
opt="--quiet ../spec2006CPU/4_gobmk_viking2.input"


time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="GOBMK_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 5 ] || [ $scelta -eq 99 ]; then
echo "BZIP2"

# MISSING

###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 6 ] || [ $scelta -eq 99 ]; then
echo "NAMD"

benchmark="../spec2006CPU/6_namd"
opt="--input ../spec2006CPU/6_namd.input --iterations 1"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="NAMD_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 7 ] || [ $scelta -eq 99 ]; then
echo "H264"

benchmark="../spec2006CPU/7_h264ref"
opt="-d ../spec2006CPU/7_h264ref.cfg -p InputFile=../spec2006CPU/7_h264ref_foreman_qcif_yuv.input"


time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="H264_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 8 ] || [ $scelta -eq 99 ]; then
echo "GROMACS"

benchmark="../spec2006CPU/8_gromacs"
opt="-silent -deffnm ../spec2006CPU/8_gromacs.input.tpr"


time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="GROMACS_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 9 ] || [ $scelta -eq 99 ]; then
echo "ZEUSMP"

# MISSING

###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 10 ] || [ $scelta -eq 99 ]; then
echo "OMNETPP"

benchmark="../spec2006CPU/10_omnetpp"
opt="../spec2006CPU/omnetpp.ini"

cp ../spec2006CPU/omnetpp.ini ./
mkdir outputs_all

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC 

filename="OMNET_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 11 ] || [ $scelta -eq 99 ]; then
echo "BWAVES"

# MISSING


###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 12 ] || [ $scelta -eq 99 ]; then
echo "ASTAR"

benchmark="../spec2006CPU/12_astar"
opt="../spec2006CPU/12_astar.input.cfg" #NB COPIARE FILES IN CARTELLA
mem_size=2048MB


cp ../spec2006CPU/12_astar.input.cfg ../spec2006CPU/12_astar.input.bin ./ #NB

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC --mem-size=$mem_size

filename="ASTAR_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 14 ] || [ $scelta -eq 99 ]; then
echo "LESLIE3D"

benchmark="../spec2006CPU/14_leslie3d"
opt="../spec2006CPU/14_leslie3d.input"
mem_size=4096MB

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" --input="$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC --mem-size=$mem_size

filename="LESLIE_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 15 ] || [ $scelta -eq 99 ]; then
echo "LBM"

benchmark="../spec2006CPU/15_lbm"
opt="20 ../spec2006CPU/15_lbm.in 0 1 ../spec2006CPU/15_lbm_100_100_130_ldc.of"
mem_size=8GB


time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" -o "$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt;$opt" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC --mem-size=$mem_size

filename="LBM_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

elif [ $scelta -eq 16 ] || [ $scelta -eq 99 ]; then
echo "GEMS"

benchmark="../spec2006CPU/16_gemsfttd"
cp ../spec2006CPU/16_gemsfttd_* ./
mv 16_gemsfttd_yee.dat yee.dat
mv 16_gemsfttd_ref.in ref.in
mv 16_gemsfttd_sphere.pec sphere.pec


time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --cpu-type=$CPUMODE --mem-type=$MEMMODE --num-cpus=$num_cpus --cmd "$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark;$benchmark" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC

filename="GEMS_stable_"${MEM_LATENCY}"_"${L2_ASSOC}"assoc"
cp m5out/stats.txt ./outputs/${filename} 
###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

fi
