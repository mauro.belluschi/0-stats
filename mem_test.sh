# channel width 8B,4vcs,timing cpu model MESI
SUFFIX='opt'
PROTOCOL='ALPHA_MESI_Two_Level'
CPUMODE='timing'
PYTHON='ruby_mem_test.py'
gem5_home='.'
NUM_CPUS=16
NUM_L2S=16
NUM_DIRS=4
NUM_ROWS=4
R=5
L1D_SIZE=16kB
L1I_SIZE=16kB
L2_SIZE=256kB
L1D_ASSOC=2
L1I_ASSOC=2
L2_ASSOC=2
TOPOLOGY=MeshDirCorners
#NUM_CPUS_PER_TILE=1
#CACHELINE_SIZE=


name='hello'
benchname=tests/test-progs/hello/bin/alpha/linux/${name}

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS}  --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE  --num-cpus-per-tile=${NUM_CPUS_PER_TILE}
