# channel width 8B,4vcs,timing cpu model MESI
SUFFIX='fast'
PROTOCOL='ALPHA_MESI_Two_Level'
CPUMODE='detailed'
PYTHON='se_noListeners.py'
gem5_home='.'
NUM_CPUS=32
NUM_L2S=16
NUM_DIRS=4
NUM_ROWS=4
R=5
L1D_SIZE=16kB #32
L1I_SIZE=16kB #32
L2_SIZE=256kB #512 
L1D_ASSOC=4
L1I_ASSOC=4
L2_ASSOC=4
TOPOLOGY=MeshDirCornersEnhanced
NUM_CPUS_PER_TILE=2
#CACHELINE_SIZE=

mkdir m5out_all
mkdir outputs_all

echo 0.01 > k.txt



name='hello'
benchname=tests/test-progs/hello/bin/alpha/linux/${name}

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name}
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs


name='susan'
benchname=../benchmarks/mibench_automotive/${name}/${name}

input_str="../benchmarks/mibench_automotive/${name}/input_small.pgm ./m5out/susan_out1.png -c"
for i in {2..16}; do input_str=$input_str";../benchmarks/mibench_automotive/susan/input_small.pgm ./m5out/susan_out$i.png -c"; done;

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name}
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

####name='patricia'
####benchname="../benchmarks/mibench_network/${name}/${name}"
####echo "testing $benchname ..."
####input_str="../benchmarks/mibench_network/${name}/small.udp"
####
####time $gem5_home/build/ALPHA_MESI_CMP_directory/gem5.${SUFFIX} $gem5_home/configs/example/se_alpha_ruby_loop.py --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str;$input_str" --caches --l2cache --num-dirs=4 --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --simulation-time=${SIM_TIME} --sample-time=${SAMPLE_TIME} --internal-sampling-freq=1ps --router-frequency=$FREQ --clock=$FREQ --vcs-per-vnet=$VCS --mesh-rows=4
####
####mv m5out m5out_all/${name}

name='qsort'
benchname="../benchmarks/mibench_automotive/${name}/${name}_small"
echo "testing $benchname ..."
input_str="../benchmarks/mibench_automotive/${name}/input_small.dat"

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name}
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

echo "testing sha..."
name=sha
name_folder=sha
benchname="../benchmarks/mibench_security/${name_folder}/${name}"

input_str='../benchmarks/mibench_security/sha/input_small.asc'
for i in {2..16}; do input_str="$input_str;../benchmarks/mibench_security/sha/input_small.asc"; done

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name} 
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

echo "testing fft..."
name='fft'
nameBIG='FFT'
benchname="../benchmarks/mibench_telecomm/${nameBIG}/${name}"

input_str="4 4096"
for i in {2..16}; do input_str=$input_str";4 4096"; done;

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name}
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

#####echo "testing crc32..."
#####name='crc'
#####nameBIG='CRC32'
#####benchname="../benchmarks/mibench_telecomm/${nameBIG}/${name}"
#####
#####input_str='../benchmarks/mibench_telecomm/adpcm/data/large.pcm'
#####for i in {2..16}; do input_str="$input_str;../benchmarks/mibench_telecomm/adpcm/data/large.pcm"; done
#####
#####time $gem5_home/build/ALPHA_MESI_CMP_directory/gem5.${SUFFIX} $gem5_home/configs/example/se_alpha_ruby_loop.py --cpu-type=$CPUMODE --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o $input_str --caches --l2cache --num-dirs=4 --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --simulation-time=${SIM_TIME} --sample-time=${SAMPLE_TIME} --internal-sampling-freq=1ps --router-frequency=$FREQ --clock=$FREQ --vcs-per-vnet=$VCS --mesh-rows=4
#####
#####mv m5out m5out_all/${name}



echo "testing search_small..."
name='search_small'
nameBIG='stringsearch'
benchname="../benchmarks/mibench_office/${nameBIG}/${name}"

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name}
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

echo "testing dijkstra_small..."
name='dijkstra'
benchname="../benchmarks/mibench_network/${name}/${name}_small"

input_str='../benchmarks/mibench_network/dijkstra/input.dat'
for i in {2..16}; do input_str="$input_str;../benchmarks/mibench_network/dijkstra/input.dat"; done

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name}
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

echo "testing bitcount 1000..."
name='bitcnts'
nameBIG='bitcount'
benchname="../benchmarks/mibench_automotive/${nameBIG}/${name}"

input_str='75000'
for i in {2..16}; do input_str="$input_str;75000"; done

techo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name}
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

echo "testing basichmath small..."
name='basicmath_small'
nameBIG='basicmath'
benchname="../benchmarks/mibench_automotive/${nameBIG}/${name}"

echo "testing $benchname ..."
time $gem5_home/build/${PROTOCOL}/gem5.${SUFFIX} $gem5_home/configs/example/${PYTHON} --cpu-type=${CPUMODE} --num-cpus=${NUM_CPUS} --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" --caches --l2cache --num-dirs=${NUM_DIRS} --num-l2caches=$NUM_L2S --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --mesh-rows=${NUM_ROWS} --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC #--cacheline_size=$CACHELINE_SIZE

mv m5out m5out_all/${name} 
mv outputs outputs_all/${name}
rm -r outputs
mkdir outputs

###name=bf
###name_folder=blowfish
###benchname="../benchmarks/mibench_security/${name_folder}/${name}"
###
###input_str="e ../benchmarks/mibench_security/${name_folder}/input_small.asc ../benchmarks/mibench_security/${name_folder}/output_small9.enc 1234567890abcdeffedcba0987654321"
###for i in {2..16}; do input_str="$input_str;e ../benchmarks/mibench_security/${name_folder}/input_small.asc ../benchmarks/mibench_security/${name_folder}/output_small9.enc 1234567890abcdeffedcba0987654321"; done
###
###time $gem5_home/build/ALPHA_MESI_CMP_directory/gem5.$SUFFIX $gem5_home/configs/example/se_alpha_ruby_loop.py --cpu-type=detailed --num-cpus=16 --cmd "$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname;$benchname" -o "$input_str" --caches --l2cache --num-dirs=4 --num-l2caches=16 --ruby --topology ${TOPOLOGY} --num-cpus-per-tile=${NUM_CPUS_PER_TILE} --garnet-network fixed --simulation-time=250ms --sample-time=250ms --internal-sampling-freq=1ps --router-frequency=$FREQ --clock=$FREQ --vcs-per-vnet=$VCS --mesh-rows=4
###
###mv m5out m5out_all/${name}
