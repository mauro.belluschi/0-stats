#!/bin/bash

SUFFIX=.fast

CPUMODE='detailed'

PYTHON='se_splash2_stable.py'

gem5_home='.'
R=5

num_cpus=16

mesh_type='MeshDirCorners'

mesh_rows=4

num_l2=16

mem_size=4GB

L1D_SIZE=16kB #32
L1I_SIZE=16kB #32
L2_SIZE=256kB #512 
L1D_ASSOC=4
L1I_ASSOC=4
L2_ASSOC=4

#l2linesize=64

mkdir m5out_all

echo 0.04 > k.txt


#MENU Choose protocol
PS3='Please enter your choice: '
options=("MOESI" "MESI" "MOESI_HAMMER" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "MOESI")
            echo "MOESI"
            PROTOCOL=MOESI_CMP_directory
            break
            ;;
        "MESI")
            echo "MESI"
            PROTOCOL=MESI_Two_Level
            break
            ;;
        "MOESI_HAMMER")
            echo "MOESI_HAMMER"
            PROTOCOL=MOESI_hammer
            break
            ;;
        "Quit")
            exit
            ;;
        *) echo invalid option;;
    esac
done

#MENU choose splash2 benchmark
PS3='Please enter your choice: '
options=("OCEAN" "RADIX" "LU" "WATER-SPATIAL" "RAYTRACE" "CHOLESKY" "FFT" "WATER-NSQUARED" "BARNES" "FMM" "ALL" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "OCEAN")
            scelta=1
            break
            ;;
        "RADIX")
            scelta=2
            break
            ;;
        "LU")
            scelta=3
            break
            ;;
        "WATER-SPATIAL")
            scelta=4
            break
            ;;
        "RAYTRACE")
            scelta=5
            break
            ;;
        "CHOLESKY")
            scelta=6
            break
            ;;
        "FFT")
            scelta=7
            break
            ;;
        "WATER-NSQUARED")
            scelta=8
            break
            ;;
        "BARNES")
            scelta=9
            break
            ;;
        "FMM")
            scelta=10
            break
            ;;
        "ALL")
            scelta=99
            break
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done

if [ $scelta -eq 1 ] || [ $scelta -eq 99 ]; then
echo "OCEAN-contiguous"
name="OCEAN"
rootdir="../v1-splash-alpha/splash2/codes/apps/ocean/contiguous_partitions/"
bench_name="$rootdir${name}"
cmd_options="-p${num_cpus}"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 2 ] || [ $scelta -eq 99 ]; then
echo "RADIX"
name=RADIX
rootdir="../v1-splash-alpha/splash2/codes/kernels/radix/"
bench_name="$rootdir${name}"
cmd_options="-p${num_cpus} -n524288"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 3 ] || [ $scelta -eq 99 ]; then
echo "LU"
name="LU"
rootdir="../v1-splash-alpha/splash2/codes/kernels/lu/contiguous_blocks/"
bench_name="$rootdir${name}"
cmd_options="-p${num_cpus}"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 4 ] || [ $scelta -eq 99 ]; then
echo "WATER-SPATIAL"
name="WATER-SPATIAL"
rootdir="../v1-splash-alpha/splash2/codes/apps/water-spatial/"
bench_name="$rootdir${name}"
cmd_options=""

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --input="${rootdir}input.p${num_cpus}" --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 5 ] || [ $scelta -eq 99 ]; then
echo "RAYTRACE"
name="RAYTRACE"
rootdir="../v1-splash-alpha/splash2/codes/apps/raytrace/"
bench_name="$rootdir${name}"
cmd_options="-p64 ./inputs/car.env"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 6 ] || [ $scelta -eq 99 ]; then
echo "CHOLESKY"
name="CHOLESKY"
rootdir="../v1-splash-alpha/splash2/codes/kernels/cholesky/"
bench_name="$rootdir${name}"
cmd_options="-p${num_cpus} ./inputs/tk14.O"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 7 ] || [ $scelta -eq 99 ]; then
echo "FFT"
name="FFT"
rootdir="../v1-splash-alpha/splash2/codes/kernels/fft/"
bench_name="$rootdir${name}"
cmd_options="-p${num_cpus} -m16"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 8 ] || [ $scelta -eq 99 ]; then
echo "WATER-NSQUARED"
name="WATER-NSQUARED"
rootdir="../v1-splash-alpha/splash2/codes/apps/water-nsquared/"
bench_name="$rootdir${name}"
cmd_options=""

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --input="${rootdir}input.p${num_cpus}" --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 9 ] || [ $scelta -eq 99 ]; then
echo "BARNES"
name="BARNES"
rootdir="../v1-splash-alpha/splash2/codes/apps/barnes/"
bench_name="$rootdir${name}"
cmd_options=""

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --input="${rootdir}input.p${num_cpus}" --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}

elif [ $scelta -eq 10 ] || [ $scelta -eq 99 ]; then
echo "FMM"
name="FMM"
rootdir="../v1-splash-alpha/splash2/codes/apps/fmm/"
bench_name="$rootdir${name}"
cmd_options="./inputs/input.2048.p${num_cpus}"

time $gem5_home/build/ALPHA_$PROTOCOL/gem5$SUFFIX $gem5_home/configs/example/$PYTHON --input="${rootdir}/inputs/input.2048.p${num_cpus}" --rootdir=${rootdir} --mem-size=$mem_size --cpu-type=detailed --num-cpus=$num_cpus --cmd=$bench_name --options="$cmd_options" --caches --l2cache --num-dirs=4 --num-l2caches=$num_l2 --ruby --topology $mesh_type --garnet-network fixed --mesh-rows=$mesh_rows --l1d_size=$L1D_SIZE --l1i_size=$L1I_SIZE --l2_size=$L2_SIZE --l1d_assoc=$L1D_ASSOC --l1i_assoc=$L1I_ASSOC --l2_assoc=$L2_ASSOC
mv m5out m5out_all/${name}
fi
