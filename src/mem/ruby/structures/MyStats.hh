#ifndef __MEM_RUBY_STRUCTURES_MYSTATS_HH__
#define __MEM_RUBY_STRUCTURES_MYSTATS_HH__

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <ios>

#include "config/statistics.hh"
#include "config/print_statistics.hh"

#include "base/callback.hh"

#include "mem/protocol/CacheRequestType.hh"
#include "mem/ruby/common/Address.hh"
#include "mem/ruby/common/MachineID.hh"
#include "mem/ruby/structures/CacheMemory.hh"

class MyStats: SimObject
{
  public:

    MyStats(const Params *p, int  l2s, int  r, std::string name):
    SimObject(p), statsType(STATISTICS), printType(PRINT_STATISTICS)
    {
      if(statsType!="NO_STATS")
      {
        num_l2 = l2s;
        rows = r;
        columns = num_l2/rows;
        localStr=name;

        m_totalAccesses=0;
        totalFetchesToMemory=0;

        Callback *cb = new MakeCallback<MyStats,
                                &MyStats::printAll>(this);
        registerExitCallback(cb);
              
        if(statsType=="ALL" || statsType=="PER_SET")
          this->schedule(new StatsEvent(this, 10000000), curTick()+1 ); 
      }
    }

    void dataFromMemoryStats(Address addr)
    {
      if(statsType=="ALL" || statsType=="DIRECTIONAL")     
        stats[addr].dataFromMemoryTick=curTick();
    }

    void updateStats(Address addr, MachineID local, MachineID requestor)
    {
        m_totalAccesses++;
      if(statsType=="ALL" || statsType=="DIRECTIONAL")
      {     

        int localPosition=local.num;
        int reqPosition=requestor.num;

        if (stats.count(addr)==0)
        {
         stats[addr].maxConseqAccesses=0;
         stats[addr].cpuMaxConseqAccesses=0; 
        }

        if(reqPosition==stats[addr].lastCpuMaxConseqAccesses)
        {
          stats[addr].lastMaxConseqAccesses++;
          if(stats[addr].lastMaxConseqAccesses > stats[addr].maxConseqAccesses)
          {
            stats[addr].maxConseqAccesses = stats[addr].lastMaxConseqAccesses;
            stats[addr].cpuMaxConseqAccesses = stats[addr].lastCpuMaxConseqAccesses;
          }
        }
        else
        {
          stats[addr].lastMaxConseqAccesses=1;
          stats[addr].lastCpuMaxConseqAccesses=reqPosition;
        }

        if(localPosition==reqPosition)
        {
          stats[addr].local++;
          stats[addr].localAccesses++;
          return;
        }

        //managing X position
        int posX1 = localPosition%columns;
        int posX2 = reqPosition%columns;
        int diffX = posX1 - posX2;

        int hopX = abs(diffX);
        if(diffX >0)
        {
          stats[addr].xMinus = stats[addr].xMinus + hopX;
          stats[addr].xMinusAccesses++; 
        }
        else
        {
          stats[addr].xPlus = stats[addr].xPlus + hopX; 
          stats[addr].xPlusAccesses++;
        }

        //managing Y position
        int count = localPosition;
        int posY1 = rows;
        int posY2 = rows;

        do
        {
          count=count-columns;
          posY1--;
        }
        while(count>=0);

        count=reqPosition;

        do
        {
          count=count-columns;
          posY2--;
        }
        while(count>=0);

        int diffY = posY1 - posY2;
        int hopY = abs(diffY);
        
        if(diffY >0)
        {
          stats[addr].yMinus = stats[addr].yMinus + hopY;
          stats[addr].yMinusAccesses++;
        }
        else
        {
          stats[addr].yPlus = stats[addr].yPlus + hopY;
          stats[addr].yPlusAccesses++;
        }

        assert(hopX!=0 || diffY!=0);
      }
      return;
    }

    void updatePerSetStats( int64 set, MachineID local, MachineID requestor)
    {
      if(statsType=="PER_SET" || statsType=="ALL")
      {    
        perSetStats[set].totAccesses++;
        perSetStatsGlobal[set].totAccesses++;
      }

      return;
    }

    void printAll()
    {
      if(statsType!="NO_STATS")
      {
        if(statsType=="ALL" || statsType=="DIRECTIONAL")
        {

          if(printType=="AT_THE_END")
          {
            /*##########################################################*/
            //writing stats of replaced blocks on file
            if(o.str().compare("")!=0)
            {
              std::string nameOfFile1;
              nameOfFile1.append("outputs/REPLACEMENT_");
              nameOfFile1.append( localStr );
              nameOfFile1.append(".csv"); 
              std::ofstream output1( nameOfFile1, std::ofstream::out | std::ios::app);
              assert (output1.is_open());

              output1 << o.str();

              output1.close();
            }
          }

          /*##########################################################*/
          //writing stats of NOT replaced blocks to a file
          if(stats.begin()!=stats.end())
          {
            std::string nameOfFile2;
            nameOfFile2.append("outputs/NOREPLACEMENT_");
            nameOfFile2.append( localStr );
            nameOfFile2.append(".csv"); 
            std::ofstream output2( nameOfFile2, std::ofstream::out );
            assert (output2.is_open());

            m5::hash_map<Address, StatStruct>::const_iterator it;


            for(it=stats.begin(); it!=stats.end(); ++it)
            {
              output2 << it->second.dataFromMemoryTick <<"\t";
              output2 << curTick() <<"\t"<< it->first;

              output2 << "\t"<< it->second.xPlus <<"\t"<< it->second.xMinus <<"\t"<< it->second.yPlus << "\t"<< it->second.yMinus;
              output2 << "\t"<< it->second.local;

              output2 << "\t"<< it->second.xPlusAccesses <<"\t"<< it->second.xMinusAccesses <<"\t"<< it->second.yPlusAccesses << "\t"<< it->second.yMinusAccesses;
              output2 << "\t"<< it->second.localAccesses;


              output2 << "\t"<< it->second.maxConseqAccesses <<"\t"<< it->second.cpuMaxConseqAccesses <<"\n";
            }
            output2.close();
          }
        }


        /*##########################################################*/
        /**********writing PER SET stats*********/
        
        if(statsType=="ALL" || statsType=="PER_SET")
        {
          if(perSetStatsGlobal.begin()!=perSetStatsGlobal.end())
          {
            std::string nameOfFile3;
            nameOfFile3.append("outputs/PERSET_");
            nameOfFile3.append( localStr );
            nameOfFile3.append(".csv"); 
            std::ofstream output3( nameOfFile3, std::ofstream::out );
            assert (output3.is_open());

            m5::hash_map<int64, PerSetStatStruct>::const_iterator it2;


            for(it2=perSetStatsGlobal.begin(); it2!=perSetStatsGlobal.end(); ++it2)
            {
              output3 << it2->first;

              output3 << "\t"<< it2->second.totAccesses <<"\t"<< it2->second.totFetches << "\t"<< it2->second.totReplacements << "\n";
            }
            output3.close();
          }

          /*##########################################################*/
          /**********writing PER SET PERIODIC stats*********/
          if(printType=="AT_THE_END")
          {
            if(perSetStatsVect.begin()!=perSetStatsVect.end())
            {
              std::string nameOfFile4;
              nameOfFile4.append("outputs/PERSET_PERIODIC_");
              nameOfFile4.append( localStr );
              nameOfFile4.append(".csv"); 
              std::ofstream output4( nameOfFile4, std::ofstream::out );
              assert (output4.is_open());

              m5::hash_map<int64, std::vector<PerSetStatStruct>>::const_iterator it4;

              for(it4=perSetStatsVect.begin(); it4!=perSetStatsVect.end(); ++it4)
              {
                output4 << it4->first;

                std::vector<PerSetStatStruct>::const_iterator it4b;

                for(it4b=it4->second.begin(); it4b!=it4->second.end(); ++it4b)
                {
                  output4 << "\t"<< it4b->totAccesses <<"\t"<< it4b->totFetches << "\t"<< it4b->totReplacements << "\t";
                }
                
                output4 << "\n";
              }
              output4.close();
            }
          }
        }

        /*##########################################################*/
        //writing global stats
        if(m_totalAccesses!=0 && totalFetchesToMemory!=0)
        {
          std::string nameOfFile4;
          nameOfFile4.append("outputs/GLOBAL_");
          nameOfFile4.append( localStr );
          nameOfFile4.append(".csv"); 
          std::ofstream output4( nameOfFile4, std::ofstream::out );
          assert (output4.is_open());

          output4 << m_totalAccesses <<"\n"<< totalFetchesToMemory;

          output4.close();
        }
      
    }
  }


    void resetMyStats(Address addr, int64 set, MachineID local)
    {
      if(statsType=="ALL" || statsType=="PER_SET")
      {
        perSetStats[set].totReplacements++;
        perSetStatsGlobal[set].totReplacements++;
      }


      if(statsType=="ALL" || statsType=="DIRECTIONAL")
      {
        stats[addr].totReplacements++;


        if(printType=="AT_THE_END")
        {

          o << stats[addr].dataFromMemoryTick << "\t";
          o << curTick() << "\t" << addr;

          o << "\t"<< stats[addr].xPlus <<"\t"<< stats[addr].xMinus <<"\t"<< stats[addr].yPlus << "\t"<< stats[addr].yMinus;
          o << "\t"<< stats[addr].local;

          o << "\t"<< stats[addr].xPlusAccesses <<"\t"<< stats[addr].xMinusAccesses <<"\t"<< stats[addr].yPlusAccesses << "\t"<< stats[addr].yMinusAccesses;
          o << "\t"<< stats[addr].localAccesses;

          o << "\t"<< stats[addr].maxConseqAccesses <<"\t"<< stats[addr].cpuMaxConseqAccesses <<"\n";
        }
        else if(printType=="DURING_SIMULATION")
        {
          std::string nameOfFile;
          nameOfFile.append("outputs/REPLACEMENT_");
          nameOfFile.append( localStr );
          nameOfFile.append(".csv"); 
          std::ofstream output( nameOfFile, std::ofstream::out | std::ios::app);
          assert (output.is_open());

          output << stats[addr].dataFromMemoryTick << "\t";
          output << curTick() << "\t" << addr;

          output << "\t"<< stats[addr].xPlus <<"\t"<< stats[addr].xMinus <<"\t"<< stats[addr].yPlus << "\t"<< stats[addr].yMinus;
          output << "\t"<< stats[addr].local;

          output << "\t"<< stats[addr].xPlusAccesses <<"\t"<< stats[addr].xMinusAccesses <<"\t"<< stats[addr].yPlusAccesses << "\t"<< stats[addr].yMinusAccesses;
          output << "\t"<< stats[addr].localAccesses;

          output << "\t"<< stats[addr].maxConseqAccesses <<"\t"<< stats[addr].cpuMaxConseqAccesses <<"\n";

          output.close();
        }

        //reset stats
        stats[addr].xPlus=0;
        stats[addr].yPlus=0;
        stats[addr].xMinus=0;
        stats[addr].yMinus=0;
        stats[addr].local=0;

        stats[addr].xPlusAccesses=0;
        stats[addr].yPlusAccesses=0;
        stats[addr].xMinusAccesses=0;
        stats[addr].yMinusAccesses=0;
        stats[addr].localAccesses=0;

        stats[addr].maxConseqAccesses=0;
        stats[addr].lastMaxConseqAccesses=0;
        stats[addr].cpuMaxConseqAccesses=0;
        stats[addr].lastCpuMaxConseqAccesses=0;
        stats[addr].dataFromMemoryTick=0;
      }
    }

    void countFetches (Address addr, int64 set)
    {
      if(statsType=="PER_SET" || statsType=="ALL")
      {
          perSetStats[set].totFetches++;
          perSetStatsGlobal[set].totFetches++;
      }
      if(statsType=="DIRECTIONAL" || statsType=="ALL")
          stats[addr].totFetches++;

      if(statsType!="NO_STATS")
          totalFetchesToMemory++;
    }


    struct StatStruct
    {
      int xPlus;
      int yPlus;
      int xMinus;
      int yMinus;
      int local;

      int xPlusAccesses;
      int yPlusAccesses;
      int xMinusAccesses;
      int yMinusAccesses;
      int localAccesses;

      int maxConseqAccesses;
      int cpuMaxConseqAccesses;
      int lastMaxConseqAccesses;
      int lastCpuMaxConseqAccesses;
      int dataFromMemoryTick;

      int totAccesses;
      int totFetches;
      int totReplacements;
    };

    struct PerSetStatStruct
    {
      int totAccesses;
      int totFetches;
      int totReplacements;
    };

   private:
    m5::hash_map<Address, StatStruct> stats;

    m5::hash_map<int64, PerSetStatStruct> perSetStats;
    m5::hash_map<int64, PerSetStatStruct> perSetStatsGlobal;
    m5::hash_map<int64, std::vector<PerSetStatStruct>> perSetStatsVect;

    class StatsEvent : public Event
    {
      public:
          StatsEvent(MyStats* m, Tick t)
              : Event(Default_Pri, AutoDelete)
          {
            ticks=t;
            myStats=m;
          }


          void process() 
          {

            m5::hash_map<int64, PerSetStatStruct>::const_iterator it;

            if(myStats->printType=="AT_THE_END")
            {

              for(it=myStats->perSetStats.begin(); it!=myStats->perSetStats.end(); ++it)
              {

                myStats->perSetStatsVect[it->first].push_back(it->second);

                myStats->perSetStats[it->first].totAccesses=0;
                myStats->perSetStats[it->first].totFetches=0;
                myStats->perSetStats[it->first].totReplacements=0;
              }
            }
            else if(myStats->printType=="DURING_SIMULATION")
            {
              if(myStats->perSetStats.begin()!=myStats->perSetStats.end())
              {
                std::string nameOfFile;
                nameOfFile.append("outputs/PERSET_PERIODIC_");
                nameOfFile.append( myStats->localStr );
                nameOfFile.append(".csv"); 
                std::ofstream outf( nameOfFile, std::ofstream::out | std::ios::app);
                assert (outf.is_open());

                for(it=myStats->perSetStats.begin(); it!=myStats->perSetStats.end(); ++it)
                {
                  outf << it->first << "\t";
                }

                for(it=myStats->perSetStats.begin(), outf << "\n"; it!=myStats->perSetStats.end(); ++it)
                {
                  outf << it->second.totAccesses << "\t";
                  myStats->perSetStats[it->first].totAccesses=0;
                }

                for(it=myStats->perSetStats.begin(), outf << "\n"; it!=myStats->perSetStats.end(); ++it)
                {
                  outf << it->second.totFetches << "\t";
                  myStats->perSetStats[it->first].totFetches=0;
                }

                for(it=myStats->perSetStats.begin(), outf << "\n"; it!=myStats->perSetStats.end(); ++it)
                {
                  outf << it->second.totReplacements << "\t";
                  myStats->perSetStats[it->first].totReplacements=0;
                }

                outf << "\n\n";
                outf.close();
              }
            }

            myStats->schedule(new StatsEvent(myStats, ticks), curTick()+ticks ); 
          }

      private:
          MyStats* myStats;
          Tick ticks;
    };

   /**
    * Number of L2 caches
    */
    int num_l2;

   /**
    * Number of rows in the mesh
    */
    int rows;

   /**
    * Number of columns in the mesh
    */
    int columns;

    int totalFetchesToMemory;

  public:
    std::stringstream  o;

    int m_totalAccesses;

    std::string statsType;
    std::string printType;

    std::string localStr;
};

#endif // __MEM_RUBY_STRUCTURES_MYSTATS_HH__