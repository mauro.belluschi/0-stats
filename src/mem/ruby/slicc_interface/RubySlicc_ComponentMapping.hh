/*
 * Copyright (c) 1999-2008 Mark D. Hill and David A. Wood
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __MEM_RUBY_SLICC_INTERFACE_RUBYSLICC_COMPONENTMAPPINGS_HH__
#define __MEM_RUBY_SLICC_INTERFACE_RUBYSLICC_COMPONENTMAPPINGS_HH__

#include "mem/protocol/MachineType.hh"
#include "mem/ruby/common/Address.hh"
#include "mem/ruby/common/MachineID.hh"
#include "mem/ruby/common/NetDest.hh"
#include "mem/ruby/structures/DirectoryMemory.hh"

#include "config/private_mapping.hh" //modified

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

// used to determine the home directory
// returns a value between 0 and total_directories_within_the_system
inline NodeID
map_Address_to_DirectoryNode(const Address& addr)
{
    return DirectoryMemory::mapAddressToDirectoryVersion(addr);
}

// used to determine the home directory
// returns a value between 0 and total_directories_within_the_system
inline MachineID
map_Address_to_Directory(const Address &addr)
{
    MachineID mach =
        {MachineType_Directory, map_Address_to_DirectoryNode(addr)};
    return mach;
}

inline NetDest
broadcast(MachineType type)
{
    NetDest dest;
    for (NodeID i = 0; i < MachineType_base_count(type); i++) {
        MachineID mach = {type, i};
        dest.add(mach);
    }
    return dest;
}

inline MachineID
mapAddressToRange(const Address & addr, MachineType type, int low_bit,
                  int num_bits, int cluster_id = 0)
{
    MachineID mach = {type, 0};
    if (num_bits == 0)
        mach.num = cluster_id;
    else
        mach.num = addr.bitSelect(low_bit, low_bit + num_bits - 1)
            + (1 << num_bits) * cluster_id;
    return mach;
}

inline NodeID
machineIDToNodeID(MachineID machID)
{
    return machID.num;
}

inline MachineID
mapAddressToRangeMacrotiles(const Address & addr, MachineID source, MachineType type, int low_bit)
{
#if PRIVATE_MAPPING==1
    MachineID m = {type, source.num};  
    
    return m;
#endif
    //Constructing the vector related to the NoC and macrotiles    
    std::ifstream fin_1("macrotiles.txt");
    assert(fin_1.is_open());
    
    std::string line1;
    int index = 0;
    int index_noc = 0;
    std::map < int, int > macrotiles;

    while(getline(fin_1,line1))
        {
        std::stringstream linestream(line1);
        std::string line2;

        while(getline(linestream,line2,' '))
        {
            std::stringstream linestream(line2);
            std::string value1;
            if( source.num == index_noc)
            {
                while(getline(linestream,value1,','))
                {
                    macrotiles[index] = atoi(value1.c_str());
                    index ++;
                }
            }
        index_noc ++;
        }
    }

    fin_1.close();

    int num_bits = log2 (index);
    int n_of_tiles = pow (2.0, num_bits);

    if( n_of_tiles < index ){
        num_bits = num_bits + 1;
        int num_unassigned_indexes = pow(2, num_bits) - (index);

        for(int count=0; count < num_unassigned_indexes; count ++)
	   {
		  macrotiles[index] = macrotiles[count];
          index ++;
	   }
    }

    MachineID mach = {type, 0};

    if (num_bits == 0)
        return mach;
    //Finally, it returns the absolute position of the destination node, after taking its relative position (the position in the macrotile) w.r.t. the bit selected
    int dest_tile_id = (int)addr.bitSelect(low_bit, low_bit + num_bits - 1);
    mach.num = macrotiles[dest_tile_id];
    assert(mach.num >= 0 && mach.num <= 15);

    return mach;
}

inline MachineType
machineIDToMachineType(MachineID machID)
{
    return machID.type;
}

inline int
machineCount(MachineType machType)
{
    return MachineType_base_count(machType);
}

inline MachineID
createMachineID(MachineType type, NodeID id)
{
    MachineID mach = {type, id};
    return mach;
}

#endif  // __MEM_RUBY_SLICC_INTERFACE_COMPONENTMAPPINGS_HH__
